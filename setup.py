from setuptools import setup,find_packages

setup(
    name="dopy",
    version="0.3.7",
    py_modules=[
        'manager',
    ],
    install_requires=[
        "requests"
    ]
)